import java.util.*;
import java.io.*;
/**
 * Write a description of class RedeSocial here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class RedeSocial implements Serializable{
    
    TreeMap<String,Utilizador> rede;
    
    /**
     * Constructor for objects of class RedeSocial
     */
    public RedeSocial(){
        rede=new TreeMap<String,Utilizador>();
        
    }
    
    public RedeSocial(RedeSocial r){
        rede=r.getRede();
    }

    public TreeMap<String,Utilizador> getRede(){
        TreeMap<String,Utilizador> aux=new TreeMap<String,Utilizador>();
        for(Utilizador u:rede.values())
            aux.put(u.getMail(),u.clone());
        return aux;
    }
    
    public void setRede(TreeMap<String,Utilizador> r){
        TreeMap<String,Utilizador> aux=new TreeMap<String,Utilizador>();
        for(Utilizador u:r.values())
            aux.put(u.getMail(),u.clone());
        rede=aux;
    }
    
    public void addUtilizador(Utilizador u)throws UtilizadorExisteException{
        if(rede.containsKey(u.getMail()))
            throw new UtilizadorExisteException(u.getMail());
        rede.put(u.getMail(),u.clone());
    }
    
    public void removeUtilizador(Utilizador u) throws UtilizadorExisteException{
        if(!rede.containsKey(u.getMail()))
            throw new UtilizadorExisteException(u.getMail());
        rede.remove(u.getMail());
    }
    
    public int login(String email,String pass) throws UtilizadorExisteException{
        if(!rede.containsKey(email))
            throw new UtilizadorExisteException(email);
        if(rede.get(email).getPass().equals(pass))
            return 1;
        else return 0;
    }
    
    public void gravaObj(String fich) throws IOException {
      ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fich));
      oos.writeObject(this);
      oos.flush(); oos.close();
    } 
    
    public Utilizador getUser(String u){
        return rede.get(u).clone();
    }
    
    public void enviaPedido(String u,String a) throws UtilizadorExisteException, AmigoExisteException{
        if(!rede.containsKey(a))
            throw (new UtilizadorExisteException());
        if(rede.get(u).getAmigos().contains(rede.get(a)))
            throw (new AmigoExisteException());
        
        rede.get(a).addPedido(rede.get(u));
        
    }
    
    public int diffMes(Actividade a){
        GregorianCalendar today = new GregorianCalendar();
    
        GregorianCalendar dt_activ = new GregorianCalendar();

        dt_activ.set(Calendar.MONTH, today.get(Calendar.MONTH));

        int diff = today.get(Calendar.MONTH) - a.getDt_pratica().get(Calendar.MONTH);

        return diff;
    }
    
    public int diffAnos(Actividade a){
        GregorianCalendar today = new GregorianCalendar();
    
        GregorianCalendar dt_activ = new GregorianCalendar();

        dt_activ.set(Calendar.YEAR, today.get(Calendar.YEAR));

        int diff = today.get(Calendar.YEAR) - a.getDt_pratica().get(Calendar.YEAR);
        return diff;
    }
   
    public float estMensalTem(String u,String dp){
        float tot=0;
        if(dp.equals("todas")){
            for(Actividade a: rede.get(u).getActiv_real()){
                if( diffMes(a)==0 )
                    tot=tot+a.getDuracao();
                }
        }else{
            for(Actividade a: rede.get(u).getActiv_real()){
                if(a.getClass().getSimpleName().equals(dp) && diffMes(a)==0)
                    tot=tot+a.getDuracao();
            }
        
        }
        return tot;
    }
    
    public float estAnualTem(String u,String dp){
        float tot=0;
        if(dp.equals("todas")){
            for(Actividade a: rede.get(u).getActiv_real()){
                if( diffAnos(a)<1)
                    tot=tot+a.getDuracao();
                }
        }else{
            for(Actividade a: rede.get(u).getActiv_real()){
                if(a.getClass().getSimpleName().equals(dp) && diffAnos(a)==0)
                    tot=tot+a.getDuracao();
        }
        
        }
        return tot;
    }
    
    public float estTotTem(String u,String dp){
        float tot=0;
        if(dp.equals("todas")){
            for(Actividade a: rede.get(u).getActiv_real()){
                tot=tot+a.getDuracao();
                }
        }else{
            for(Actividade a: rede.get(u).getActiv_real()){
                if(a.getClass().getSimpleName().equals(dp))
                    tot=tot+a.getDuracao();
        }
        
        }
        return tot;
    }
    
    
    public float estMensalDis(String u,String dp){
        float tot=0;
        if(dp.equals("todas")){
            for(Actividade a: rede.get(u).getActiv_real()){
                if( diffMes(a)==0 && a instanceof Vel_Dis){
                  Vel_Dis aux=(Vel_Dis) a;
                  tot=tot+aux.getDistancia();
                }
                }
        }else{
            for(Actividade a: rede.get(u).getActiv_real()){
             if(a.getClass().getSimpleName().equals(dp) && diffMes(a)==0 && a instanceof Vel_Dis ){
                Vel_Dis aux2=(Vel_Dis) a;
                tot=tot+aux2.getDistancia();
            }
        }   
        }
        return tot;
    }
    
    public float estAnualDis(String u,String dp){
        float tot=0;
        if(dp.equals("todas")){
            for(Actividade a: rede.get(u).getActiv_real()){
                if( diffAnos(a)==0 && a instanceof Vel_Dis){
                  Vel_Dis aux=(Vel_Dis) a;
                  tot=tot+aux.getDistancia();}
                }
        }else{
            for(Actividade a: rede.get(u).getActiv_real()){
             if(a.getClass().getSimpleName().equals(dp) && diffAnos(a)==0 && a instanceof Vel_Dis){
                Vel_Dis aux2=(Vel_Dis) a;
                tot=tot+aux2.getDistancia();
            }
        }   
    }
    return tot;
    }
    
    public float estTotDis(String u,String dp){
        float tot=0;
        if(dp.equals("todas")){
            for(Actividade a: rede.get(u).getActiv_real()){
                  if(a instanceof Vel_Dis){
                    Vel_Dis aux=(Vel_Dis) a;
                    tot=tot+aux.getDistancia();}
                }
        }else{
            for(Actividade a: rede.get(u).getActiv_real()){
             if(a.getClass().getSimpleName().equals(dp)&& a instanceof Vel_Dis){
                 Vel_Dis aux2=(Vel_Dis) a;
                tot=tot+aux2.getDistancia();
            }
        }   
    }
    return tot;
    }
    
    public double estMensalCal(String u,String dp){
        double tot=0;
        if(dp.equals("todas")){
            for(Actividade a: rede.get(u).getActiv_real()){
                if( diffMes(a)==0)
                    tot=tot+a.calc_calorias(rede.get(u).getPeso());
                }
        }else{
            for(Actividade a: rede.get(u).getActiv_real()){
           if(a.getClass().getSimpleName().equals(dp) && diffMes(a)==0)
                tot=tot+a.calc_calorias(rede.get(u).getPeso());
           }
        }   
        return Math.round(tot*100/100);
    }
    
    public double estAnualCal(String u,String dp){
        double tot=0;
        if(dp.equals("todas")){
            for(Actividade a: rede.get(u).getActiv_real()){
                    if( diffAnos(a)< 1)
                        tot=tot+a.calc_calorias(rede.get(u).getPeso());
                }
        }else{
            for(Actividade a: rede.get(u).getActiv_real()){
                if(a.getClass().getSimpleName().equals(dp) && diffAnos(a)==0)
                    tot=tot+a.calc_calorias(rede.get(u).getPeso());
           }
        }   
        return Math.round(tot*100/100);
    }
    
    public double estTotCal(String u,String dp){
        double tot=0;
        if(dp.equals("todas")){
            for(Actividade a: rede.get(u).getActiv_real()){
                   
                        tot=tot+a.calc_calorias(rede.get(u).getPeso());
                }
        }else{
            for(Actividade a: rede.get(u).getActiv_real()){
                if(a.getClass().getSimpleName().equals(dp))
                    tot=tot+a.calc_calorias(rede.get(u).getPeso());
           }
        }   
        return Math.round(tot*100/100);
    }
    
    
    
    public void replaceUtilizador (Utilizador u){
        rede.put(u.getMail(),u.clone());
    }
    
    public void newRecord(String u,Actividade a){
        if(!rede.get(u).getRecordes().containsKey(a.getClass().getSimpleName()))
            rede.get(u).addRecorde(a);
        if(a instanceof Vel_Dis){
            Vel_Dis aux1=(Vel_Dis) a;
            Vel_Dis aux2=(Vel_Dis) rede.get(u).getRecordes().get(a.getClass().getSimpleName());
            if(aux1.calcVel_media()>aux2.calcVel_media())
                rede.get(u).addRecorde(a);
        }
        if(a instanceof Tem_Pontos){
            Tem_Pontos aux3=(Tem_Pontos) a;
            Tem_Pontos aux4=(Tem_Pontos) rede.get(u).getRecordes().get(a.getClass().getSimpleName());
            if(aux3.getPontos()>aux4.getPontos())
                rede.get(u).addRecorde(a);
        }
        
    }
    
    public RedeSocial clone(){
        return new RedeSocial(this);
    }
    
    public boolean equals(Object o){
        if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;}
        RedeSocial a=(RedeSocial) o;
        int cont=0;
        for(Utilizador u:rede.values()){
            if(u.equals(a.getUser(u.getMail())))
                cont++;
            }
        if(cont==this.rede.size())
            return true;
        return false;
        
    }
    
    
     public String toString(){  
        StringBuilder s = new StringBuilder(); 
        for(Utilizador u:this.rede.values()){
            s.append(u.toString());
        }
       s.append(System.lineSeparator());
       return s.toString();
    }
}
