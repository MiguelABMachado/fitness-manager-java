import java.util.*;
import java.io.*;
import java.text.*;

/**
 * Write a description of class Pilates here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */



public class Pilates extends Actividade implements Serializable  {
   
    public Pilates () {
        super();
    }
    
    public Pilates ( int duracao, GregorianCalendar dt) {
        super( duracao, dt);
    }
    
    public Pilates(Pilates p){
        super(p);
    }
 
   
    
    public boolean equals(Object o){
        return super.equals(o);
        
    }
    
    
     public String toString(){
        StringBuilder s = new StringBuilder(); 
        s.append("Actividade:Pilates");
        s.append(System.lineSeparator());
        s.append("Duração: ");
        s.append(getDuracao());
        s.append(" minutos");
        s.append(System.lineSeparator());
        s.append("Praticada em: ");

        SimpleDateFormat aux= new SimpleDateFormat("dd/MM/yyyy");
        String auxS=aux.format(getDt_pratica().getTime());
        s.append(auxS.toString());
        s.append(System.lineSeparator());
        return s.toString();
    }
  
    public Pilates clone() {
        return new Pilates(this);
    }
    
      public double calc_calorias (float p){
        return (0.057*getDuracao()*p);
    }
   
}
