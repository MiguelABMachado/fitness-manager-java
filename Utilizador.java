import java.util.*;
import java.io.*;
import java.text.*;
/**
 * Write a description of class Utilizador here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Utilizador implements Serializable{
    // instance variables 
    private String email;
    private String pass;
    private String nome;
    private String genero;
    private float altura;
    private float peso;
    private GregorianCalendar dt_nascimento;
    private String dp_fav;//desporto favorito 
    private TreeSet<Actividade> activ_real;
    
    private TreeSet<Utilizador> amigos;
    
    private ArrayList<Utilizador> pedidos;
    private TreeMap<String,Actividade> recordes;
    
 
    /**
     * Constructor for objects of class Utilizador
     */
    //Construtor Vazio/////////////////////////////////////////////
    public Utilizador(){
        email="";
        pass="";
        nome="";
        genero="";
        altura=0;
        peso=0;
        dt_nascimento=null;
        dp_fav="";
        activ_real=new TreeSet<Actividade>(new ActividadeComparator());
        amigos=new TreeSet<Utilizador>(new UtilizadorComparator());
        
        pedidos=new ArrayList<Utilizador>();
        recordes=new TreeMap<String,Actividade>();
        
    }
     // Construtor Parametrizado////////////////////////////
    public Utilizador(String e,String password,String n,String g,float a,float p,GregorianCalendar dt,String df){//Ao começar utilizador nao tem amigos nem atividades
        email=e;
        pass=password;
        nome=n;
        genero=g;
        altura=a;
        peso=p;
        dt_nascimento=dt;
        dp_fav=df;
        activ_real=new TreeSet<Actividade>(new ActividadeComparator());
        amigos=new TreeSet<Utilizador>(new UtilizadorComparator());
        
        pedidos=new ArrayList<Utilizador>();
        recordes=new TreeMap<String,Actividade>();
    }
    
    //Construtor de Cópia///////////////////////////////////////////////////////
    
    public Utilizador(Utilizador u){
        email=u.getMail();
        pass=u.getPass();
        nome=u.getNome();
        genero=u.getGenero();
        altura=u.getAltura();
        peso=u.getPeso();
        dt_nascimento=u.getDtnasc();
        dp_fav=u.getDp_fav();
        activ_real=u.getActiv_real();
        amigos=u.getAmigos();
        pedidos=u.getPedidos();
        recordes=u.getRecordes();
    }
    
    
    
    
   //Getters e Setters
   
   
    public String getMail(){
        return this.email;
    }
  
    public String getPass(){
        return this.pass;
    }
  
  
    public String getNome(){
        return this.nome;
    }
  
  
    public String getGenero(){
        return this.genero;
    }
  
  
    public float getAltura(){
        return this.altura;
    }
  
    public float getPeso(){
        return this.peso;
    }
  
  
    public GregorianCalendar getDtnasc(){
        return (GregorianCalendar) this.dt_nascimento.clone();  
    }
  
  
  
    public String getDp_fav(){
        return this.dp_fav;
    }
  
    public TreeSet<Actividade> getActiv_real(){
        TreeSet<Actividade> aux=new TreeSet<Actividade>(new ActividadeComparator());
        
        for(Actividade a : activ_real)
            aux.add(a.clone());
        return aux;
    }
  
  
    public TreeSet<Utilizador> getAmigos(){
       TreeSet<Utilizador> amigos_list=new TreeSet<Utilizador>(new UtilizadorComparator());
       Iterator<Utilizador> it=this.amigos.iterator();
       while(it.hasNext())
            amigos_list.add(it.next());
       return amigos_list;
    }
    
   
      
      public ArrayList<Utilizador> getPedidos(){
        
        ArrayList<Utilizador> aux= new ArrayList<Utilizador>();
        
        for(Utilizador a : pedidos)
            aux.add(a.clone());
        return aux;
    }
    
    public TreeMap<String,Actividade> getRecordes(){
        TreeMap<String,Actividade> aux=new TreeMap<String,Actividade>();
        
        for(Actividade a : this.recordes.values())
            aux.put(a.getClass().getSimpleName(),a.clone());
        return aux;
    }
    
  
  
    public void setMail(String x){
        this.email=x;
    }

    public void setPass(String x){
        this.pass=x;
    }
  
    public void setNome(String x){
        this.nome=x;
    }
  
    public void setGenero(String x){
        this.genero=x;
    }

    public void setAltura(float x){
        this.altura=x;
    }

    public void setPeso(float x){
        this.peso=x;
    }
  
    public void setDt_nasc(GregorianCalendar x){
        this.dt_nascimento= (GregorianCalendar) x.clone();
    }

    public void setDp_fav(String x){
        this.dp_fav=x;
    }
  
    public void setActiv_real(TreeSet<Actividade> x){
        TreeSet<Actividade> aux=new TreeSet<Actividade>(new ActividadeComparator());
        Iterator<Actividade> it=x.iterator();
        while(it.hasNext()){
            aux.add(it.next());
        }
        activ_real=aux;
    }
  
  
    public void setAmigos(TreeSet <Utilizador> x){
       TreeSet<Utilizador> amigos_list=new TreeSet<Utilizador>(new UtilizadorComparator());
       Iterator<Utilizador> it=x.iterator();
       while(it.hasNext()){
            amigos_list.add(it.next());
       }
       amigos=amigos_list;
    }
    
    public void setPedidos(ArrayList<Utilizador> x){
        ArrayList<Utilizador> aux=new ArrayList<Utilizador>();
        for(Utilizador u:x)
            aux.add(u.clone());
        pedidos=aux;
    }
    
    public void setRecordes(TreeMap<String,Actividade> x){
        TreeMap<String,Actividade> aux =new TreeMap<String,Actividade>();
        for(Actividade a:x.values())
            aux.put(a.getClass().getSimpleName(),a.clone());
        recordes=aux;
    }
    
    
    
    
    /**
     * equals,clone,toString
     */
     public boolean equals(Object o){
        if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;}
        Utilizador a=(Utilizador) o;
        return (this.getMail().equals(a.getMail()));
    }
    
    public Utilizador clone(){
        return new Utilizador(this);
    }
    
    public String toString(){  
        StringBuilder s = new StringBuilder(); 
        s.append("Email: ");
        s.append(email);
        s.append(System.lineSeparator());
        s.append("Nome: ");
        s.append(nome);
        s.append(System.lineSeparator());
        s.append("Genero ");
        s.append(genero);
        s.append(System.lineSeparator());
        s.append("Altura: ");
        s.append(altura);
        s.append(System.lineSeparator());
        s.append("Peso: ");
        s.append(peso);
        s.append(System.lineSeparator());
        s.append("Data de nascimento: ");
        SimpleDateFormat aux= new SimpleDateFormat("dd/MM/yyyy");
        String auxS=aux.format(dt_nascimento.getTime());
        s.append(auxS.toString());
        s.append(System.lineSeparator());
        return s.toString();
    }
    
   /**
     * outras cenas
     */
    
    public void addActividade(Actividade a){
        this.activ_real.add(a);
    }
    
    public void addPedido(Utilizador u){
        this.pedidos.add(u);
    }
    
    
    
    public void removeActividade(Actividade a){
         this.activ_real.remove(a);
    }
    
    
    //!!!!!!!!!!!!!!!!!!!1
    public void removeAmigo(String email){
        this.amigos.remove(email);
    }
    //!!!!!!!!!!!!!!!!!
    
    public int removePedido (int x){
        this.pedidos.remove(x);
        return x-1;
    }
    
    
    public void addAmigo(Utilizador a){
        this.amigos.add(a.clone());
    }
    
     
    public void remAmigo(Utilizador a){
        this.amigos.remove(a);
    }
    
    
    public int getAge(){
    GregorianCalendar today = new GregorianCalendar();
    
    GregorianCalendar bdayThisYear = new GregorianCalendar();

    bdayThisYear.set(Calendar.YEAR, today.get(Calendar.YEAR));

    int age = today.get(Calendar.YEAR) - getDtnasc().get(Calendar.YEAR);

    if(today.getTimeInMillis() < bdayThisYear.getTimeInMillis())
        age--;

    return age;
   }
   
   public void addRecorde(Actividade a){
       this.recordes.put(a.getClass().getSimpleName(),a);
    }
    
    
        
        
 }


