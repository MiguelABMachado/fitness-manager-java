import java.util.*;
import java.io.*;

public abstract class JogoIndoor extends Actividade implements Serializable
{
    // instance variables - replace the example below with your own
    private int resultado;
    private int pontos;
    
  
  public JogoIndoor() {
        super();
        this.resultado = 0;
        this.pontos=0;
  }
  
  public JogoIndoor(int resultado, int duracao, GregorianCalendar dt,int p){
      super( duracao, dt);
      this.resultado=resultado;
      this.pontos=p;
  }
  
  public JogoIndoor(JogoIndoor j) {
      super(j);
      this.resultado = j.getResultado();
      this.pontos=j.getPontos();
   
  }
  
  
  
  
  public int getResultado() {
      return this.resultado;
  }
  
  public void setResultado(int r) {
      this.resultado = r;
  }
  
  public int getPontos() {
      return this.pontos;
  }
  
  public void setPontos(int r) {
      this.pontos = r;
  }
  
  
    public boolean equals(Object o){
       return super.equals(o);
        
   }
  
  
  
  
  public abstract JogoIndoor clone();
    
}
