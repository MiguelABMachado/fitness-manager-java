import java.util.*;
import java.io.*;

public class UtilizadorExisteException extends Exception implements Serializable{
    public UtilizadorExisteException(){
        super();
    }
    
    public UtilizadorExisteException(String s){
        super(s);
    }
}
