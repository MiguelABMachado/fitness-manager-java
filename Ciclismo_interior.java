import java.util.*;
import java.io.*;
import java.text.*;
public  class Ciclismo_interior extends Corridas_Indoor implements Serializable{
    
    

    public Ciclismo_interior(){
        super();
    }
    
    public Ciclismo_interior(int distancia,  int duracao, GregorianCalendar dt){
        super(distancia,  duracao, dt);
    }
    
    
    public Ciclismo_interior (Ciclismo_interior c){
        super(c);
    }
    // calcular calorias gastas
    
    
    /*public double calc_calorias (float p, int idade){
         return (132.6 - (0.17 * p) - (0.39 * idade) - (3.27*getDuracao()) - (0.156 * 230 - idade));
    }*/
    
      public double calc_calorias (float p){
        return 0.133*getDuracao()*p;
         
        
    }
   
    

    
    
    // toString, clone e equals
    
    
   public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("Desporto:Ciclismo Interior.");
        s.append(System.lineSeparator());
        s.append("Duração: ");
        s.append(getDuracao());
        s.append(" minutos.");
        s.append(System.lineSeparator());
        s.append("Distancia: ");
        s.append(getDistancia());
        s.append("quilómetros.");
        s.append(System.lineSeparator());
        s.append("Velocidade media: ");
        s.append(calcVel_media());
        s.append("quilómetros/hora");
        s.append(System.lineSeparator());
        s.append("Velocidade média: ");
        s.append(calcVel_media());
        s.append(" Km/h");
        s.append(System.lineSeparator());
        s.append("Praticada em: ");

        SimpleDateFormat aux= new SimpleDateFormat("dd/MM/yyyy");
        String auxS=aux.format(getDt_pratica().getTime());
        s.append(auxS.toString());
        s.append(System.lineSeparator());
        return s.toString();
        
    } 
    
   
   public boolean equals(Object o){
       return super.equals(o);
        
   }
   
   
   public Ciclismo_interior clone(){
       return new Ciclismo_interior(this);
    }
      

}

