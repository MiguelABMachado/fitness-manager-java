import java.util.*;
import java.io.*;
import java.text.*;
public abstract class Actividade implements Serializable{
    
    // variaveis de instancia
    
    private int duracao;
    private GregorianCalendar dt_pratica;


    //construtor vazio
    public Actividade(){
       
        this.duracao=0;
        this.dt_pratica=new GregorianCalendar();
    }
    
    //contrutor paramatrizado
    public Actividade(/*String nome, /*int cal_gastas,*/ int duracao,GregorianCalendar dt){
        
        this.duracao=duracao;
        this.dt_pratica= (GregorianCalendar) dt.clone();
    }
    
    
    //construtor de cópia
    public Actividade(Actividade a){
       
        this.duracao=a.getDuracao();
        this.dt_pratica=a.getDt_pratica();
    }
    
    
    //getter e setter
     
    
    public GregorianCalendar getDt_pratica(){
        return (GregorianCalendar) this.dt_pratica.clone();
    }
    
    
  
    
    
    
  
    public int getDuracao(){
        return this.duracao;
    }
    
    public void setDt_pratica(GregorianCalendar dt){
        this.dt_pratica=(GregorianCalendar)dt.clone();
    }
    
  
    
    
    
    
    public void setDuracao(int x){
        this.duracao=x;
    }
    
    
     
    // toString, clone e equals
     
    public boolean equals(Object o){
        if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;}
        Actividade a=(Actividade) o;
        return (this.dt_pratica.equals(a.getDt_pratica())&&(this.duracao==a.getDuracao()));
        
    }
    
    public abstract Actividade clone();
    
    public abstract String toString();
       
    
    public abstract double calc_calorias(float p);
    
}
