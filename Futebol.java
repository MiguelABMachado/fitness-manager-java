import java.util.*;
import java.io.*;
import java.text.*;
public class Futebol extends Jogos_outdoor implements Serializable{
 
    //RECORD MAIOR NUMERO DE VITORIAS SEGUIDAS
    
       //construtor vazio
    public Futebol(){
        super();
        
    }    
    
   
    //contrutor paramatrizado
    public Futebol(int r, String est_tempo, int temp_media,  int duracao, GregorianCalendar dt,int p){
        super(r,est_tempo, temp_media, duracao, dt,p); 
    }
    
    
    //construtor de cópia
    public Futebol(Futebol c){
        super(c);
    }
    
   
    public double calc_calorias (float p){
        return (0.166*getDuracao()*p);
    }
   
   

   
   
  
    // toString, clone e equals
    
    
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("Desporto:Futebol");
        s.append(System.lineSeparator());
        s.append("Duração: ");
        s.append(getDuracao());
        s.append(" minutos");
        s.append(System.lineSeparator());
        s.append("Estado do tempo: ");
        
        s.append(getEst_tempo());
        s.append(System.lineSeparator());
        s.append("Temperatura Media: ");
        
        s.append(getTemp_media());
        s.append(" graus");
        s.append(System.lineSeparator());
        if (getResultado() == -1) {
         s.append("Derrota");
        }
        if (getResultado() == 0) {
          s.append("Empate");
        }
        if (getResultado() == 1) {
          s.append("Vitória");
        }
        s.append(System.lineSeparator());
        s.append("Pontos: ");
        s.append(getPontos());
        s.append(System.lineSeparator());
        s.append("Praticada em: ");

        SimpleDateFormat aux= new SimpleDateFormat("dd/MM/yyyy");
        String auxS=aux.format(getDt_pratica().getTime());
        s.append(auxS.toString());
        s.append(System.lineSeparator());
        return s.toString();
       
    }
    
    
   
   public boolean equals(Object o){
       return super.equals(o);
        
   }
   
    public  Futebol clone(){
        return new Futebol(this);
    }
      


}
