
import java.util.*;
import java.io.*;


public class UtilizadorComparator implements Comparator<Utilizador>,Serializable {
    
    public int compare(Utilizador a1 ,Utilizador a2){
        if(a1.equals(a2)) return 0;
        if(a1.getNome().compareTo(a2.getNome())!= 0) return a1.getNome().compareTo(a2.getNome());
        return 1;
    }
}