import java.util.*;
import java.io.*;


public abstract class Corridas_altimetria extends Corridas_outdoor implements Serializable{
   
    // variaveis de instancia
    private int altimetria;
    
       //construtor vazio
    public Corridas_altimetria(){
        super();
        this.altimetria =0;
    }    
    
   
    //contrutor paramatrizado
    public Corridas_altimetria(int altimetria, int distancia,  String estado_tempo, int temperatura_media,  int duracao, GregorianCalendar dt){
        super(distancia,  estado_tempo, temperatura_media,  duracao, dt); 
        this.altimetria=altimetria;

    }
    
    
    //construtor de cópia
    public Corridas_altimetria(Corridas_altimetria c){
        super(c);
        this.altimetria =c.getAltimetria();
    }
    
    
    //getter e setter
     
        
  
   public int getAltimetria(){
        return this.altimetria;
    }
    
   public void setAltimetria(int d){
        this.altimetria=d;
    }
   
  
 
    
   
    
   
    

    
    //  clone e equals
    
    
   
    
   
  public boolean equals(Object o){
        return super.equals(o);
    }
  
    
    public abstract Corridas_altimetria clone();
      

}
