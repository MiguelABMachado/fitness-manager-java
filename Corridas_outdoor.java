import java.util.*;
import java.io.*;


public abstract class Corridas_outdoor extends Outdoor implements Serializable{
   
    // variaveis de instancia
    private float distancia;
    
    
       //construtor vazio
    public Corridas_outdoor(){
        super();
        this.distancia =0;
        
    }    
    
   
    //contrutor paramatrizado
    public Corridas_outdoor(float distancia,  String estado_tempo, int temperatura_media, int duracao, GregorianCalendar dt){
        super(estado_tempo, temperatura_media,  duracao, dt); 
        this.distancia =distancia;
        
    }
    
    
    //construtor de cópia
    public Corridas_outdoor(Corridas_outdoor c){
        super(c);
        this.distancia =c.getDistancia();
        
    }
    
    
    //getter e setter
     

  
    public float getDistancia(){
        return this.distancia;
    }
    
   public void setDistancia(int d){
        this.distancia=d;
    }
   
    
    
    
  
    
    
    
    
    
    // toString, clone e equals
    
    
   
    
   
   public boolean equals(Object o){
        return super.equals(o);
        
    }
   
    public abstract Corridas_outdoor clone();
      

}
