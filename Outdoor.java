import java.util.*;
import java.io.*;



public abstract class Outdoor extends Actividade implements Serializable{
    
    // variaveis de instancia
    private String estado_tempo;
    private int temperatura_media;


    //construtor vazio
    public Outdoor(){
        super();
        this.estado_tempo ="";
        this.temperatura_media=0;
    }
    
    //contrutor paramatrizado
    public Outdoor(String estado_tempo, int temperatura_media,  int duracao, GregorianCalendar dt){
        super(duracao, dt);
        this.estado_tempo =estado_tempo;
        this.temperatura_media=temperatura_media;
    }
    
    
    //construtor de cópia
    public Outdoor(Outdoor o){
        super(o);
        this.estado_tempo =o.getEst_tempo();
        this.temperatura_media=o.getTemp_media();
    }
    
    
    //getter e setter
     
    
    
    public String getEst_tempo(){
        return this.estado_tempo;
    }
    
    public int getTemp_media(){
        return this.temperatura_media;
    }
    
    
    public void setEst_tempo(String s){
        this.estado_tempo= s;
    }
   
    public void setTemp_media(int t){
        this.temperatura_media=t;
    }
   
    
     
    
    
    
    // clone e equals
     
    
 
    
    
    
    public abstract Actividade clone();
        
         
        
    public boolean equals(Object o){
        return super.equals(o);
    }
    
    
        
}
