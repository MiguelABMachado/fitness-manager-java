import java.util.*;
import java.io.*;
import java.text.*;
public class Basket extends JogoIndoor implements Serializable{
 
      
       //construtor vazio
    public Basket(){
        super();
        
    }    
    
   
    //contrutor paramatrizado
    public Basket(int r,  int duracao, GregorianCalendar dt,int p){
        super(r, duracao, dt,p); 
    }
    
    
    //construtor de cópia
    public Basket(Basket c){
        super(c);
    }
    
    

   
    public double calc_calorias (float p){
        return (0.2*getDuracao()*p);
    }
   
   
   

   
   
  
    // toString, clone e equals
    
    
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("Desposto:Basquetebol.");
        s.append(System.lineSeparator());
        s.append("Duração: ");
        s.append(getDuracao());
        s.append(" minutos");
        s.append(System.lineSeparator());
        
        if (getResultado() == -1) {
         s.append("Derrota");
       }
       if (getResultado() == 0) {
          s.append("Empate");
       }
       if (getResultado() == 1) {
          s.append("Vitória");
       }
       s.append(System.lineSeparator());
       s.append("Pontos: ");
       s.append(getPontos());
       s.append(System.lineSeparator());
       s.append("Praticada em: ");

        SimpleDateFormat aux= new SimpleDateFormat("dd/MM/yyyy");
        String auxS=aux.format(getDt_pratica().getTime());
        s.append(auxS.toString());
        s.append(System.lineSeparator());
        return s.toString();
      
    }
    
    
   
   public boolean equals(Object o){
       return super.equals(o);
        
   }
   
    public  Basket clone(){
        return new Basket(this);
    }
      


}