import java.util.*;
import java.io.*;
import java.text.*;

public class Natacao extends Corridas_outdoor implements Serializable{
    
    

       //construtor vazio
    public Natacao(){
        super();
    }    
    
   
    //contrutor paramatrizado
    public Natacao(int distancia,  String estado_tempo, int temperatura_media,int duracao, GregorianCalendar dt){
        super(distancia,  estado_tempo, temperatura_media,  duracao, dt); 

    }
    
    public double calc_calorias (float p){
        return (0.15*getDuracao()*p);
    }
   
    
   public float calcVel_media(){
       float v;
       v=  this.getDistancia()/this.getDuracao()*60;
       return v;
    }
    
    
    
    
    // toString, clone e equals
    
    
   public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("Desporto:Natação");
        s.append(System.lineSeparator());
        s.append("Duração: ");
        s.append(getDuracao());
        s.append(" minutos");
        s.append(System.lineSeparator());
        s.append("Estado do tempo: ");
        
        s.append(getEst_tempo());
        s.append(System.lineSeparator());
        s.append("Temperatura Media: ");
       
        s.append(getTemp_media());
        s.append(" graus");
        s.append(System.lineSeparator());
        s.append("Distancia: ");
        
        s.append(getDistancia());
        s.append(" quilómetros");
        s.append(System.lineSeparator());
        s.append("Velocidade média: ");
        s.append(calcVel_media());
        s.append(" Km/h");
        s.append(System.lineSeparator());
        s.append("Praticada em: ");

        SimpleDateFormat aux= new SimpleDateFormat("dd/MM/yyyy");
        String auxS=aux.format(getDt_pratica().getTime());
        s.append(auxS.toString());
        s.append(System.lineSeparator());
        return s.toString();
        
    }
    
  
  public boolean equals(Object o){
        return super.equals(o);
        
    }
 
    
   public Natacao clone(){
       return new Natacao();
    }
      

}