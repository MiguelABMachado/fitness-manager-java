import java.util.*;
import java.io.*;

public abstract class Jogos_outdoor extends Outdoor implements Serializable{
   
    // variaveis de instancia
    private int resultado;
    private int pontos;

    
       //construtor vazio
    public Jogos_outdoor(){
        super();
        this.resultado =0;
        this.pontos =0;

        
    }    
    
   
    //contrutor paramatrizado
    public Jogos_outdoor(int r, String est_tempo, int temp_media, int duracao, GregorianCalendar dt,int p){
        super(est_tempo, temp_media,  duracao, dt); 
        this.resultado =r;
        this.pontos=p;
    }
    
    
    //construtor de cópia
    public Jogos_outdoor(Jogos_outdoor c){
        super(c);
        this.resultado=c.getResultado();
        this.pontos=c.getPontos();

    }
    
    

    
    //getter e setter
     
    public int getPontos() {
      return this.pontos;
  }
  
  public void setPontos(int r) {
      this.pontos = r;
  }


    
    public int getResultado(){
        return this.resultado;
    }
   
    

   
   public void setResultado(int p){
        this.resultado=p;
   }

   
   
   
   
  
    //  clone e equals
    
    
    
    
    
   
   public boolean equals(Object o){
       return super.equals(o);
        
   }
   
    public abstract Jogos_outdoor clone();
      

}
