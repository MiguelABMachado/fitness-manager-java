import java.io.*;
import java.util.*;
/**
 * Write a description of class FitnessUMapp here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class FitnessUMapp{
    private static RedeSocial rede= new RedeSocial();
    private static Menu menuLogin,menuUtilizador,menuActividades,menuOpActiv,menuPedAmiz,menuDecPed,menuAmigo,menuEstatisticas,menuDecEstatisticas;
    
    
    public static void main(String[] args){
        try{carregaDados();
        }catch(IOException e){System.out.println("IO exception");
        }catch(ClassNotFoundException e) {System.out.println("class exception");}
        carregaMenus();
        execMenuLogin();
    }
    
    
    public static void execMenuLogin(){
        do {
            limpaEcra();
            System.out.println("************ Bem-vindo a FitnessUM ************");
            menuLogin.executa();
            switch (menuLogin.getOpcao()) {
                case 1:eLogin();
                       
                case 2:eCriaUtilizador();
                        
            }
        }while (menuLogin.getOpcao()!=0);
        sair();
    }
    
    public static void eCriaUtilizador(){
        System.out.println("Insira o email");
        String email=Input.lerString();
        System.out.println("Insira a password");
        String pass=Input.lerString();
        System.out.println("Insira o nome");
        String nome=Input.lerString();
        System.out.println("Insira o genero");
        String genero=Input.lerString();
        System.out.println("Insira a altura");
        float altura=Input.lerFloat();
        System.out.println("Insira o peso");
        float peso=Input.lerFloat();
        System.out.println("Introduza a sua data de nascimento na ordem dia,mês,ano separado por Enters");
        int dia=Input.lerInt();
        int mes=Input.lerInt();
        int ano=Input.lerInt();
        GregorianCalendar dt1=new GregorianCalendar(ano,(mes-1),dia);
        System.out.println("Insira o seu desporto favorito");
        String dp_favorito=Input.lerString();
        Utilizador u=new Utilizador(email,pass,nome,genero,altura,peso,dt1,dp_favorito);
        try{
            rede.addUtilizador(u);
        }
        catch(UtilizadorExisteException e){
        System.out.println("Utilizador já existe");
        mostraMensagem();
        }
        System.out.println("Utilizador criado com sucesso");
        mostraMensagem();
        execMenuLogin();
    }
    

    public static void eLogin(){
        int aux=0;
        System.out.println("Insira o email");
        String email=Input.lerString();
        System.out.println("Insira a password");
        String pass=Input.lerString();
        try{
          aux=rede.login(email,pass);
        }
        catch(UtilizadorExisteException e){
               System.out.println("Utilizador inexistente");
               mostraMensagem();
               execMenuLogin();
        }
        if(aux==0){
           System.out.println("Password Inválida");
           mostraMensagem();
           execMenuLogin();
        }
        execMenuUtilizador(email);
    }
    
    public static void execMenuUtilizador(String u){
        do {
            limpaEcra();
            menuUtilizador.executa();
            switch (menuUtilizador.getOpcao()) {
                case 1: execMenuActividades(u);
                        break;
                case 2: verActividades(u);
                        break;
                case 3: verAmigos(u);
                        break;
                case 4: execMenuPedAmiz(u);
                        break;
                     
                case 5:execMenuRecordes(u,u);
                        break;
                case 6:execMenuEstatisticas(u);
                    break;
        }
        }while (menuUtilizador.getOpcao()!=0);
            limpaEcra();
        execMenuLogin();
    }
    
    public static void execMenuRecordes(String u,String a){
        do {
            limpaEcra();
            if(rede.getUser(a).getRecordes().size()==0){
                 System.out.println("Não tem recordes");
                 mostraMensagem();
                 execMenuUtilizador(u);
            }
            menuActividades.executa();
            switch (menuActividades.getOpcao()) {
                case 1:imprimeRec(u,a,"Ciclismo_Interior");
                       break;
                case 2:imprimeRec(u,a,"Basket");
                      
                       break;
                case 3:imprimeRec(u,a,"BTT");
                       
                       break;
                case 4:imprimeRec(u,a,"Natacao");
                       
                       break;
                case 5:imprimeRec(u,a,"Futebol");
                       
                       break;
                case 6:System.out.println("Não registamos recordes para esta actividade");
                       mostraMensagem();
                       break;  
                
            }
        }while (menuActividades.getOpcao()!=0);
        execMenuUtilizador(u);
    }
    
    public static void imprimeRec(String u,String a,String b){
        limpaEcra();
        if(rede.getUser(a).getRecordes().get(b)==null){
            System.out.println("Não tem recorde nesta actividade");
            mostraMensagem();
            execMenuRecordes(u,a);
        }else{
         System.out.println(rede.getUser(a).getRecordes().get(b));
         mostraMensagem();
        }
        return;
    }
    public static void execMenuEstatisticas(String u){
        do {
            limpaEcra();
            menuEstatisticas.executa();
            switch (menuEstatisticas.getOpcao()) {
                case 1: execMenuDecEstatisticasCD(u,"todas");
                        break;
                case 2:execMenuDecEstatisticasCD(u,"Ciclismo_interior");
                        break;
                case 3: execMenuDecEstatisticas(u,"Basket");
                        break;
                case 4: execMenuDecEstatisticasCD(u,"BTT");
                        break;
                        
                case 5:execMenuDecEstatisticasCD(u,"Natacao");
                       break;
                    
                case 6:execMenuDecEstatisticas(u,"Futebol");
                        break;
                        
                case 7:execMenuDecEstatisticas(u,"Pilates");
                        break;
        }
        }while (menuEstatisticas.getOpcao()!=0);
            limpaEcra();
        execMenuUtilizador(u);
    }
    
    public static void execMenuDecEstatisticas(String u,String dp){
       do {
            limpaEcra();
            menuDecEstatisticas.executa();
            switch (menuDecEstatisticas.getOpcao()) {
                case 1: System.out.println("Este mês gastaste "+rede.estMensalCal(u,dp)+" calorias.");
                        System.out.println("Este mês gastaste "+rede.estMensalTem(u,dp)+" minutos");
                        mostraMensagem();
                        break;
                case 2: System.out.println("Este ano gastaste "+rede.estAnualCal(u,dp)+" calorias.");
                        System.out.println("Este ano gastaste "+rede.estAnualTem(u,dp)+" minutos");
                        mostraMensagem();
                        break;
                case 3: System.out.println("Ao todo gastaste "+rede.estTotCal(u,dp)+" calorias.");
                        System.out.println("Ao todo gastaste "+rede.estTotTem(u,dp)+" minutos");
                        mostraMensagem();
                        break;
                
        }
        }while (menuDecEstatisticas.getOpcao()>3);
            limpaEcra();
        execMenuEstatisticas(u);
    }
    
    public static void execMenuDecEstatisticasCD(String u,String dp){
       do {
            limpaEcra();
            menuDecEstatisticas.executa();
            switch (menuDecEstatisticas.getOpcao()) {
                case 1: limpaEcra();
                        System.out.println("Este mês gastaste "+rede.estMensalCal(u,dp)+" calorias.");
                        System.out.println("Este mês gastaste "+rede.estMensalTem(u,dp)+" minutos");
                        System.out.println("Este mês gastaste "+rede.estMensalDis(u,dp)+" quilómetros");
                        mostraMensagem();
                        break;
                case 2: limpaEcra();
                        System.out.println("Este ano gastaste "+rede.estAnualCal(u,dp)+" calorias.");
                        System.out.println("Este ano gastaste "+rede.estAnualTem(u,dp)+" minutos");
                        System.out.println("Este ano gastaste "+rede.estAnualDis(u,dp)+" quilómetros");
                        mostraMensagem();
                        break;
                case 3: limpaEcra();
                        System.out.println("Ao todo gastaste "+rede.estTotCal(u,dp)+" calorias.");
                        System.out.println("Ao todo gastaste "+rede.estTotTem(u,dp)+" minutos");
                        System.out.println("Ao todo gastaste "+rede.estTotDis(u,dp)+" quilómetros");
                        mostraMensagem();
                        break;
                    }    
            }while (menuDecEstatisticas.getOpcao()>3);
            limpaEcra();
            execMenuEstatisticas(u);
    }


    
    public static void execMenuPedAmiz(String u){
      do {
            limpaEcra();
            menuPedAmiz.executa();
            switch (menuPedAmiz.getOpcao()) {
                case 1: verPedidos(u);
                        break;
                case 2: mandarPedido(u);
                        break;
            }
        }while (menuPedAmiz.getOpcao()!=0);
        execMenuUtilizador(u);  
    }
    
    public static int execMenuAmigo(String u, String s){//u -> mail para onde vamos voltar s-> quem tamos a visitar
        int op;
        do{
             System.out.println(rede.getUser(s).toString());
             menuAmigo.executa();
            switch (op=menuAmigo.getOpcao())  {
                case 1: verFactividades(u,s);
                        break;
               case 2: verFamigos(u,s);
                        break;
               case 3: execMenuRecordes(u,s);
                        break;
               case 4: removeAmigo(u,s);
                        break;
                case 5: limpaEcra();
                        break;
            }
            
        }while (menuAmigo.getOpcao() !=0 && menuAmigo.getOpcao()!=4 && menuAmigo.getOpcao()!=5);
        limpaEcra();
        return op;
        
    }
    
    
    public static void mandarPedido(String u){
        System.out.println("Insira o email de quem quer pedir em amizade");
        String a = Input.lerString();
        try{rede.enviaPedido(u,a);}
        catch(UtilizadorExisteException e)
            {System.out.println("Utilizador não existe");
             mostraMensagem();
             execMenuPedAmiz(u);
            }
        catch (AmigoExisteException e)
            {System.out.println("O utilizador já existe na sua lista de amigos");
             mostraMensagem();
             execMenuPedAmiz(u);
            }
        System.out.println("Pedido de amizade enviado com sucesso");
        mostraMensagem();
        execMenuPedAmiz(u);
    }
    

    
    public static void execMenuActividades(String u){
        do {
            limpaEcra();
            menuActividades.executa();
            switch (menuActividades.getOpcao()) {
                case 1:ciclismo_interior(u);
                       System.out.println("Actividade criada com sucesso");
                       mostraMensagem();
                       break;
                case 2:basket(u);
                       System.out.println("Actividade criada com sucesso");
                       mostraMensagem();
                       break;
                case 3:btt(u);
                       System.out.println("Actividade criada com sucesso");
                       mostraMensagem();
                       break;
                case 4:natacao(u);
                       System.out.println("Actividade criada com sucesso");
                       mostraMensagem();
                       break;
                case 5:futebol(u);
                       System.out.println("Actividade criada com sucesso");
                       mostraMensagem();
                       break;
                case 6:pilates(u);
                       System.out.println("Actividade criada com sucesso");
                       mostraMensagem();
                       break;  
                
            }
        }while (menuActividades.getOpcao()!=0);
        execMenuUtilizador(u);
    }
    
    public static void verActividades(String u){
        TreeSet<Actividade> aux = rede.getUser(u).getActiv_real();
        limpaEcra();
        if(aux.size()==0){
            System.out.println("Não há registo de atividades");
            mostraMensagem();
            execMenuUtilizador(u);
        }else{
            Iterator<Actividade> it= aux.iterator();
            int op=1;
            int stop=0;
            while(it.hasNext() && stop <10 && op!=0){
                 op=execMenuOpActiv(u,it.next());
                 stop++;
             }
            execMenuUtilizador(u);
        }
    }
    
    
    public static void verFactividades(String u,String s){
        TreeSet<Actividade> aux = rede.getUser(s).getActiv_real();
        limpaEcra();
        if(aux.size()==0){
            System.out.println("Não há registo de atividades");
            mostraMensagem();
            limpaEcra();
            execMenuAmigo(u,s);
        }else{
            Iterator<Actividade> it= aux.iterator();
            int stop=0;
            while(it.hasNext() && stop <10){
                stop++;
                System.out.println(it.next().toString());
            }
        }
        execMenuUtilizador(u);
    }
    
    public static void removeAmigo(String u, String s){
        Utilizador aux = new Utilizador (rede.getUser(u));
        Utilizador aux2 = new Utilizador (rede.getUser(s));
        aux.remAmigo(aux2);
        aux2.remAmigo(aux);//rede.get(u) -> vai buscar à rede o utilizador em que o mail é  a string u
        rede.replaceUtilizador(aux);
        rede.replaceUtilizador(aux2);
        

    }
    
    public static void verPedidos(String u){
        ArrayList<Utilizador> aux = rede.getUser(u).getPedidos();
        limpaEcra();
        int i =0;
        if(aux.size()==0){
            System.out.println("Não tem pedidos");
            mostraMensagem();
            execMenuPedAmiz(u);
        }else{
            for(Utilizador a:aux){
                System.out.println(a.toString());
                i=execMenuDecPed(u,a,i);
                i++;
            }
        }
        execMenuPedAmiz(u);
    }
    
    public static void verAmigos(String u){
        TreeSet<Utilizador> aux = rede.getUser(u).getAmigos();
        int flag=1;
        limpaEcra();
        if(aux.size()==0){
            System.out.println("A sua lista de amigos encontra-se vazia.");
            mostraMensagem();
            execMenuUtilizador(u);
        }else{
            for(Utilizador a:aux){
                if(flag==0){
                    execMenuUtilizador(u);
                }else{
                    flag=execMenuAmigo(u,a.getMail());   
                }
            }
        }
        execMenuUtilizador(u);
    }
   
    public static void verFamigos(String u,String s){
        TreeSet<Utilizador> aux = rede.getUser(s).getAmigos();
        int flag=1;
        limpaEcra();
        if(aux.size()==0){
            System.out.println("A sua lista de amigos encontra-se vazia.");
            mostraMensagem();
            execMenuUtilizador(u);
        }else{
            for(Utilizador a:aux){
                if(flag==0){
                    execMenuUtilizador(u);
                }else{
                    
                    flag=execMenuAmigo(u,a.getMail());   
                }
            }
        }
        execMenuUtilizador(u);
    }
    
    public static int aceitarPedido (String u,Utilizador a,int x){
        //STRING U - EMAIL DE QUEM RECEBE O PEDIDO
        //UTILIZADOR A - UTILIZADOR QUE ENVIA O PEDIDO
        // add amigo nos dois e remover pedido
        // x ->posiçao
        Utilizador aux = new Utilizador (rede.getUser(u));
        aux.addAmigo(a);
        a.addAmigo(aux);//rede.get(u) -> vai buscar à rede o utilizador em que o mail é  a string u
        
        int i=aux.removePedido(x);
        rede.replaceUtilizador(aux);
        rede.replaceUtilizador(a);
        return i;
    }
    
    public static int rejeitarPedido (String u,int x){
        int i;
        Utilizador aux=new Utilizador(rede.getUser(u));
        i=aux.removePedido(x);
        rede.replaceUtilizador(aux);
        return i;
        
    }
        
    
    
    
        
    public static int execMenuDecPed(String u,Utilizador a,int x){
        do {
            menuDecPed.executa();
            switch (menuDecPed.getOpcao()) {
                case 1: x=aceitarPedido(u,a,x);
                        break;
                        
                case 2: x=rejeitarPedido(u,x);
                        break;
                        
                case 3: limpaEcra();
                        break;
                        
            }
        }while (menuDecPed.getOpcao()>3  );
        return x;
    }
    
    
    public static int execMenuOpActiv(String u,Actividade a){
        int op;
        limpaEcra();
        do {
            System.out.println(a.toString());
            menuOpActiv.executa();
            switch (op=menuOpActiv.getOpcao()) {
                case 1: limpaEcra();
                        break;
                /*case 2: eRemoveActiv(u,a);
                        break;*/
                case 2: eCalcCalorias(u,a);
                        break;
            }
        }while (menuOpActiv.getOpcao()>2);
        
        return op;
    }
    
    
    
    public static void eCalcCalorias(String u,Actividade a){
        limpaEcra();
        double cal = a.calc_calorias(rede.getUser(u).getPeso());
        System.out.println(a.toString());
        System.out.println("Nesta actividade gastastes "+cal+" calorias.");
        mostraMensagem();
        
    }
    
    public static void sair(){
       
       try{
       rede.gravaObj("RedeObj");
       }
       catch(IOException e){
        System.out.println("Não gravou os dados");   
        }
       System.exit(0);
    }
    
    public static void carregaDados() throws IOException,ClassNotFoundException{
        FileInputStream door = new FileInputStream("RedeObj"); 
        ObjectInputStream reader = new ObjectInputStream(door); 
        if(rede instanceof RedeSocial)
            rede = (RedeSocial) reader.readObject();
    }


    public static void carregaMenus(){
        String[] login={  "Login",
                          "Criar conta",
                          }; 
        
        String[] utilizador={   "Adicionar actividade",
                                "Ver lista de actividades",
                                "Ver lista de amigos",
                                "Pedidos de amizade",
                                "Ver recordes",
                                "Estatisticas pessoais"
                                };
                               
        String[] actividades={ "Ciclismo Interior",
                                "Basquetebol",
                                //"Squash",
                                //"Badminton",
                                //"Boxe",
                                //"Ténis de mesa",
                                //"Roller Ski",
                                "BTT",
                                //"Ciclismo",
                                //"Canoagem",
                                "Natação",
                                //"Remo",
                                //"Basebol",
                                //"Cricket",
                                //"Voleibol Praia",
                                "Futebol",
                                //"Tenis",
                                //"Aeróbica",
                                "Pilates"};
                                //"Eliptica",
                                //"Mergulho";
  
         String[] opActiv ={"Ver próxima actividade",
                           
                            "Calcular calorias gastas em actividade"};
                            
         String[] pedAmiz={"Ver pedidos",
                           "Enviar pedido"};
                           
         String[] decPed={"Aceitar pedido",
                          "Rejeitar pedido",
                          "Respondo mais tarde"};
         String[] amigo={"Ver lista de actividades",
                          "Ver lista de amigos",
                          "Ver recordes",
                          "Remover amigo",
                          "Ver próximo"};
                          
         String[] estatisticas={"Todas as actividades",
                                "Ciclismo Interior",
                                "Basquetebol",
                                "BTT",
                                "Natação",
                                "Futebol",
                                "Pilates"}    ;
                                
        String[] decEstatisticas={"Mensais",
                                  "Anuais",
                                  "Totais"};
       
                                  
       menuLogin=new Menu(login);
       menuUtilizador=new Menu(utilizador);
       menuActividades= new Menu(actividades);
       menuOpActiv=new Menu(opActiv);
       menuPedAmiz=new Menu(pedAmiz);
       menuDecPed=new Menu(decPed);
       menuAmigo=new Menu(amigo);
       menuEstatisticas=new Menu(estatisticas);
       menuDecEstatisticas=new Menu(decEstatisticas);
    }
    
    private final static void limpaEcra (){
       System.out.print('\u000C');
    }
    
    private static void mostraMensagem(){
        
        Scanner reader = new Scanner(System.in);
        String c;
        System.out.println("--- Prima <e> para sair ---");
        do {
            c = Input.lerString();
        }
        while(c.endsWith("e") != true);
        
    }
    
    
    
    public static void futebol(String u) {
        Utilizador k;
        System.out.println ("Introduza 1 para vitória, 0 para empate ou -1 para derrota");
        int r = Input.lerInt();
        System.out.println ("Introduza o estado do tempo");
        String est_tempo = Input.lerString();
        System.out.println ("Introduza a temperatura média em graus celsius");
        int temp_media = Input.lerInt();
        System.out.println ("Introduza a duração em minutos");
        int duracao = Input.lerInt();
        System.out.println("Introduza a data em que realizou a actividade na ordem dia,mês,ano separado por Enters");
        int dia=Input.lerInt();
        int mes=Input.lerInt();
        int ano=Input.lerInt();
        GregorianCalendar dt1= new GregorianCalendar(ano,(mes-1),dia);
        System.out.println("Introduza o número total de pontos favoraveis");
        int p = Input.lerInt();
        Futebol aux = new Futebol(r,est_tempo,temp_media,duracao,dt1,p);
        k=rede.getUser(u);
        k.addActividade(aux);
        rede.replaceUtilizador(k);
        rede.newRecord(u,aux);
        System.out.println("Actividade criada com sucesso");
        mostraMensagem();
        execMenuUtilizador(u);
    }
    
    
    public static void btt(String u) {
        Utilizador k;
        System.out.println ("Introduza a altimetria em metros");
        int altimetria = Input.lerInt();
        System.out.println ("Introduza a distância percorrida em km");
        int distancia = Input.lerInt();
        System.out.println ("Introduza o estado do tempo");
        String est_tempo = Input.lerString();
        System.out.println ("Introduza a temperatura média em graus celsius");
        int temp_media = Input.lerInt();
        System.out.println ("Introduza a duração em minutos");
        int duracao = Input.lerInt();
        System.out.println("Introduza a data em que realizou a actividade na ordem dia,mês,ano separado por Enters");
        int dia=Input.lerInt();
        int mes=Input.lerInt();
        int ano=Input.lerInt();
        GregorianCalendar dt1= new GregorianCalendar(ano,(mes-1),dia);
        BTT aux = new BTT(altimetria,distancia,est_tempo,temp_media,duracao,dt1);
        k=rede.getUser(u);
        k.addActividade(aux);
        rede.replaceUtilizador(k);
        rede.newRecord(u,aux);
        System.out.println("Actividade criada com sucesso");
        mostraMensagem();
        execMenuUtilizador(u);
    }
    
    
     public static void basket(String u) {
        Utilizador k;
        System.out.println ("Introduza 1 para vitória, 0 para empate ou -1 para derrota");
        int r = Input.lerInt();
        System.out.println ("Introduza a duração em minutos");
        int duracao = Input.lerInt();
        System.out.println("Introduza a data em que realizou a actividade na ordem dia,mês,ano separado por Enters");
        int dia=Input.lerInt();
        int mes=Input.lerInt();
        int ano=Input.lerInt();
        GregorianCalendar dt1= new GregorianCalendar(ano,(mes-1),dia);
        System.out.println("Introduza o número total de pontos favoraveis");
        int p = Input.lerInt();
        Basket aux = new Basket(r,duracao,dt1,p);
        
        k=rede.getUser(u);
        k.addActividade(aux);
        rede.replaceUtilizador(k);
        rede.newRecord(u,aux);
        System.out.println("Actividade criada com sucesso");
        mostraMensagem();
        execMenuUtilizador(u);
    }
    
    
    public static void ciclismo_interior(String u) {
        Utilizador k;
        System.out.println ("Introduza a distância percorrida em km");
        int distancia = Input.lerInt();
        System.out.println ("Introduza a duração em minutos");
        int duracao = Input.lerInt();
        System.out.println("Introduza a data em que realizou a actividade na ordem dia,mês,ano separado por Enters");
        int dia=Input.lerInt();
        int mes=Input.lerInt();
        int ano=Input.lerInt();
        GregorianCalendar dt1= new GregorianCalendar(ano,(mes-1),dia);
        Ciclismo_interior aux = new Ciclismo_interior(distancia,duracao,dt1);
        k=rede.getUser(u);
        k.addActividade(aux);
        rede.replaceUtilizador(k);
        rede.newRecord(u,aux);
        
        System.out.println("Actividade criada com sucesso");
        mostraMensagem();
        execMenuUtilizador(u);
    }
    
     
     public static void pilates(String u) {
        Utilizador k;
        System.out.println ("Introduza a duração em minutos");
        int duracao = Input.lerInt();
        System.out.println("Introduza a data em que realizou a actividade na ordem dia,mês,ano separado por Enters");
        int dia=Input.lerInt();
        int mes=Input.lerInt();
        int ano=Input.lerInt();
        GregorianCalendar dt1= new GregorianCalendar(ano,(mes-1),dia);
        Pilates aux = new Pilates(duracao,dt1);
        k=rede.getUser(u);
        k.addActividade(aux);
        rede.replaceUtilizador(k);
        System.out.println("Actividade criada com sucesso");
        mostraMensagem();
        execMenuUtilizador(u);
    }
    
     public static void natacao(String u) {
        Utilizador k;
        System.out.println ("Introduza a distância percorrida em km");
        int distancia = Input.lerInt();
        System.out.println ("Introduza o estado do tempo");
        String est_tempo = Input.lerString();
        System.out.println ("Introduza a temperatura média em graus celsius");
        int temp_media = Input.lerInt();
        System.out.println ("Introduza a duração em minutos");
        int duracao = Input.lerInt();
        System.out.println("Introduza a data em que realizou a actividade na ordem dia,mês,ano separado por Enters");
        int dia=Input.lerInt();
        int mes=Input.lerInt();
        int ano=Input.lerInt();
        GregorianCalendar dt1= new GregorianCalendar(ano,(mes-1),dia);
        Natacao aux = new Natacao(distancia,est_tempo,temp_media,duracao,dt1);
        k=rede.getUser(u);
        k.addActividade(aux);
        rede.replaceUtilizador(k);
        rede.newRecord(u,aux);
        System.out.println("Actividade criada com sucesso");
        mostraMensagem();
        execMenuUtilizador(u);
        
    }


}
    
    
 