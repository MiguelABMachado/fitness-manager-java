import java.util.*;
import java.io.*;


public class AmigoExisteException extends Exception implements Serializable{
    public AmigoExisteException(){
        super();
    }
    
    public AmigoExisteException(String s){
        super(s);
    }
}
