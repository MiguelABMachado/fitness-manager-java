import java.util.*;
import java.io.*;

public abstract class Corridas_Indoor extends Actividade implements Serializable{
   
    // variaveis de instancia
    private float distancia;
    //private int vel_media;
    
       //construtor vazio
    public Corridas_Indoor(){
        super();
        this.distancia =0;
       
        
    }    
    
   
    //contrutor paramatrizado
    public Corridas_Indoor(int distancia,  int duracao, GregorianCalendar dt){
        super( duracao, dt); 
        this.distancia =distancia;
       
    }
    
    
    //construtor de cópia
    public Corridas_Indoor(Corridas_Indoor c){
        super(c);
        this.distancia =c.getDistancia();
        

    }
    
    
    //getter e setter
     

  
    public float getDistancia(){
        return this.distancia;
    }
    
   public void setDistancia(float d){
        this.distancia=d;
    }
   
    
    
   

    

  
    
    
    // toString, clone e equals
    
    
   
    
   
   
   public boolean equals(Object o){
        if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;}
        Actividade a=(Actividade) o;
        return (equals(this.getDt_pratica().equals(a.getDt_pratica())));
        
    }
   
    public float calcVel_media(){
        return getDistancia()/getDuracao()*60 ;
    }
    
    public abstract Corridas_Indoor clone();
      

}
